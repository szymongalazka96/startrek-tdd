import game from '../src/game.js';
import config from '../src/config.js';

test('initializes 2D quadriant', () => {
  const quadriant = game.initQuadrant();
  expect(quadriant).toHaveLength(config.QUADRANT_LENGTH);
  quadriant.forEach((row) => {
    expect(row).toHaveLength(config.QUADRANT_LENGTH);
  });
});

test('initializes 2D sector', () => {
  const sector = game.initSector();
  expect(sector).toHaveLength(config.SECTOR_LENGTH);
  sector.forEach((row) => {
    expect(row).toHaveLength(config.SECTOR_LENGTH);
  });
});

test('initializes objects', () => {
  const objects = game.initObjects(10);
  expect(objects).toHaveLength(10);
  objects.forEach((object) => {
    expect(object).toEqual(
      expect.objectContaining({
        quadrant: expect.any(Array),
        sector: expect.any(Array),
      })
    );
  });
});

test.each([
  [[1, 1], [2, 2], [{ quadrant: [5, 5], sector: [5, 5] }]],
  [[1, 1], [2, 2], [{ quadrant: [1, 1], sector: [1, 1] }]],
  [[1, 1], [2, 2], [{ quadrant: [5, 5], sector: [2, 2] }]],
])('checks for collisions and evaluates to false', (quadrant, player, arr) => {
  const isCollision = game.checkCollisions(quadrant, player, arr);
  expect(isCollision).toBeFalsy();
});

test('checks for collisions and evaluates to true', () => {
  const isCollision = game.checkCollisions(
    [1, 1],
    [2, 2],
    [{ quadrant: [1, 1], sector: [2, 2] }]
  );
  expect(isCollision).toBeTruthy();
});

test.each([[[-1, 5]], [[2, -1]], [[8, 3]], [[3, 8]]])(
  'checks if player is inside boundaries and evaluates to true',
  (pos) => {
    const isOutside = game.checkBoundaries(pos);
    expect(isOutside).toBeTruthy();
  }
);

test.each([[[5, 5]], [[0, 0]], [[0, 7]], [[7, 0]], [[7, 7]]])(
  'checks if player is inside boundaries and evaluates to false',
  (pos) => {
    const isOutside = game.checkBoundaries(pos);
    expect(isOutside).toBeFalsy();
  }
);
test('calculates new Enterprise position - the same quadrant', () => {
  const {
    calcEnergy,
    newSectorXY,
    newQuadrantXY,
    calcStardates,
  } = game.calculateNewData([1, 1], [2, 2], [0, 0]);
  expect(calcEnergy).toEqual(4);
  expect(calcStardates).toEqual(0);
  expect(newQuadrantXY).toEqual([0, 0]);
  expect(newSectorXY).toEqual([3, 3]);
});
test('calculates new Enterprise position - another quadrant', () => {
  const {
    calcEnergy,
    newSectorXY,
    newQuadrantXY,
    calcStardates,
  } = game.calculateNewData([1, 1], [10, 3], [0, 0]);
  expect(calcEnergy).toEqual(13);
  expect(calcStardates).toEqual(1);
  expect(newQuadrantXY).toEqual([1, 0]);
  expect(newSectorXY).toEqual([3, 4]);
});
test('does not randomize Object positions', () => {
  const state = {
    starArr: [{ sector: [1, 1], quadrant: [1, 1] }],
    klingonArr: [],
    starfleetArr: [],
  };
  game.randomizeObjectPositions(state, [2, 2]);
  expect(state.starArr[0].sector).toEqual([1, 1]);
});

test('randomize Object positions', () => {
  const state = {
    starArr: [{ sector: [1, 1], quadrant: [2, 2] }],
    klingonArr: [],
    starfleetArr: [],
  };
  game.randomizeObjectPositions(state, [2, 2]);
  expect(state.starArr[0].sector).not.toEqual([1, 1]);
});

test('does not detect star fleet nearby', () => {
  const isNearby = game.checkStarFleetNearby(
    [1, 1],
    [2, 2],
    [{ sector: [5, 5], quadrant: [2, 2] }]
  );
  expect(isNearby).toBeFalsy();
});

test('detect star fleet nearby', () => {
  const isNearby = game.checkStarFleetNearby(
    [1, 1],
    [2, 2],
    [{ sector: [1, 2], quadrant: [2, 2] }]
  );
  expect(isNearby).toBeTruthy();
});

test('scan quadrant and report valid status', () => {
  const state = {
    starArr: [
      { sector: [1, 1], quadrant: [2, 2] },
      { sector: [1, 2], quadrant: [2, 2] },
      { sector: [1, 3], quadrant: [2, 2] },
    ],
    klingonArr: [
      { sector: [2, 1], quadrant: [5, 5] },
      { sector: [2, 1], quadrant: [2, 2] },
      { sector: [2, 2], quadrant: [2, 2] },
    ],
    starfleetArr: [
      { sector: [3, 1], quadrant: [2, 2] },
      { sector: [3, 1], quadrant: [5, 5] },
      { sector: [3, 1], quadrant: [6, 6] },
    ],
  };
  const status = game.scanQuadrant(state, [2, 2]);
  expect(status).toEqual('213');
});

test('checks if quadrant contains any klingons - true', () => {
  const state = {
    klingonArr: [
      { sector: [2, 1], quadrant: [5, 5] },
      { sector: [2, 1], quadrant: [2, 2] },
      { sector: [2, 2], quadrant: [2, 2] },
    ],
    quadrant: [2, 2],
  };
  const status = game.checkHostile(state);
  expect(status).toBeTruthy();
});

test('checks if quadrant contains any klingons - false', () => {
  const state = {
    klingonArr: [
      { sector: [2, 1], quadrant: [5, 5] },
      { sector: [2, 1], quadrant: [5, 5] },
      { sector: [2, 2], quadrant: [5, 5] },
    ],
    quadrant: [2, 2],
  };
  const status = game.checkHostile(state);
  expect(status).toBeFalsy();
});

test('shot enterprise', () => {
  const state = {
    klingonArr: [{ sector: [2, 1], quadrant: [5, 5] }],
    energy: 22,
    sector: [2, 2],
    quadrant: [5, 5],
  };
  const report = game.engage(state, true);
  expect(state.energy).toBe(2);
  expect(report).toBeFalsy();
});

test('shot klingon', () => {
  const state = {
    klingonArr: [{ sector: [2, 1], quadrant: [5, 5], energy: 30 }],
    energy: 22,
    sector: [2, 2],
    quadrant: [5, 5],
  };
  const report = game.engage(state, false, 30);
  expect(report).toBeTruthy();
});
