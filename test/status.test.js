import status from '../src/utils/status';

test('logs Enterprise status', () => {
  const log = status(30, 600, [5, 5], [4, 4]);
  expect(log).toContain(30);
  expect(log).toContain(600);
  expect(log).toContain([5, 5]);
  expect(log).toContain([4, 4]);
});
