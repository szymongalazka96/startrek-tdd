import {
  getRandomInt,
  randomXY,
  cityBlockDistance,
  isHit,
} from '../src/utils/helpers.js';
import config from '../src/config';

test('generates random int in range', () => {
  const val = getRandomInt(0, 10);
  expect(typeof val).toBe('number');
});

test('generates random coordinates', () => {
  const xy = randomXY();
  expect(xy).toHaveLength(2);
  expect(xy[0]).toBeLessThan(config.QUADRANT_LENGTH);
  expect(xy[1]).toBeLessThan(config.QUADRANT_LENGTH);
});

test.each([
  [0, 0, 1, 1, 2],
  [0, 0, -1, -1, 2],
  [0, 0, 0, 0, 0],
])(
  'calculates distance between different points in a space',
  (x1, y1, x2, y2, expected) => {
    expect(cityBlockDistance(x1, y1, x2, y2)).toBe(expected);
  }
);

test('calculates hit - 100% chance', () => {
  expect(isHit(1)).toBeTruthy();
});