import inputReader from './utils/inputReader.js';
import game from './game.js';
import config from './config.js';
import status from './utils/status.js';
import { findAllInQuadrant, getRandomInt } from './utils/helpers.js';

const COMMANDS_MODE = 0,
  MANOEUVRE_MODE = 1,
  SHORT_SCAN_MODE = 2,
  LONG_SCAN_MODE = 3,
  BATTLE_MODE = 4,
  MAP_MODE = 5;

let mode = COMMANDS_MODE; // the current mode

const state = {
  sector: config.INIT_SECTOR,
  quadrant: config.INIT_QUADRANT,
  stardates: config.INIT_STARDATES,
  energy: config.INIT_ENERGY_LVL,
  sectorArr: game.initSector(),
  quadrantArr: game.initQuadrant(),
  klingonArr: game.initObjects(config.KLINGON_COUNT),
  starfleetArr: game.initObjects(config.STARFLEET_COUNT),
  starArr: game.initObjects(config.STAR_COUNT),
  gameWon: false,
  gameLost: false,
};

state.klingonArr.forEach((klingon) => (klingon.energy = getRandomInt(50)));

while (true) {
  if (state.energy <= 0) state.gameLost = true;
  if (state.gameLost) break;
  if (mode === COMMANDS_MODE) {
    console.log(
      status(state.stardates, state.energy, state.quadrant, state.sector)
    );
    let command = inputReader.readCommand();
    switch (command) {
      case inputReader.MANOEUVRE_COMMAND:
        console.log('ENTERING INTO THE MANOEUVRE MODE');
        mode = MANOEUVRE_MODE;
        break;
      case inputReader.SHORT_SCAN_COMMAND:
        console.log('ENTERING INTO THE SHORT SCAN MODE');
        mode = SHORT_SCAN_MODE;
        break;
      case inputReader.LONG_SCAN_COMMAND:
        console.log('ENTERING INTO THE LONG SCAN MODE');
        mode = LONG_SCAN_MODE;
        break;
      case inputReader.GALAXY_MAP_COMMAND:
        console.log('ENTERING INTO THE GALAXY MAP MODE');
        mode = MAP_MODE;
        break;
      case inputReader.BATTLE_COMMAND:
        console.log('ENTERING INTO THE BATTLE MODE');
        mode = BATTLE_MODE;
        break;
    }
  } else if (mode === SHORT_SCAN_MODE) {
    const ui = [...state.sectorArr.map((row) => [...row])];
    const klingonsToDraw = findAllInQuadrant(state.klingonArr, state.quadrant);
    const starsToDraw = findAllInQuadrant(state.starArr, state.quadrant);
    const sfToDraw = findAllInQuadrant(state.starfleetArr, state.quadrant);
    ui[state.sector[0]][state.sector[1]] = '<*>';
    klingonsToDraw.forEach((kl) => (ui[kl.sector[0]][kl.sector[1]] = '+++'));
    starsToDraw.forEach((star) => (ui[star.sector[0]][star.sector[1]] = ' * '));
    sfToDraw.forEach((sf) => (ui[sf.sector[0]][sf.sector[1]] = '>!<'));

    game.drawUI(ui);
    mode = COMMANDS_MODE;
  } else if (mode === LONG_SCAN_MODE) {
    const q = state.quadrant;
    let ui = game.initQuadrant();
    let variants = [q];
    q[0] !== 7 && variants.push([q[0] + 1, q[1]]);
    q[0] !== 0 && variants.push([q[0] - 1, q[1]]);
    q[1] !== 7 && variants.push([q[0], q[1] + 1]);
    q[1] !== 0 && variants.push([q[0], q[1] - 1]);
    q[0] !== 7 && q[1] !== 7 && variants.push([q[0] + 1, q[1] + 1]);
    q[0] !== 0 && q[1] !== 0 && variants.push([q[0] - 1, q[1] - 1]);
    q[0] !== 7 && q[1] !== 0 && variants.push([q[0] + 1, q[1] - 1]);
    q[0] !== 0 && q[1] !== 7 && variants.push([q[0] - 1, q[1] + 1]);
    variants.forEach((v) => {
      const status = game.scanQuadrant(state, v);
      ui[v[0]][v[1]] = status;
      state.quadrantArr[v[0]][v[1]] = status;
    });
    game.drawUI(ui);
    mode = COMMANDS_MODE;
  } else if (mode === MAP_MODE) {
    const ui = [...state.quadrantArr.map((row) => [...row])];
    game.drawUI(ui);
    mode = COMMANDS_MODE;
  } else if (mode === MANOEUVRE_MODE) {
    let vector = inputReader.readVector();
    const {
      calcEnergy,
      newSectorXY,
      newQuadrantXY,
      calcStardates,
    } = game.calculateNewData(state.sector, vector, state.quadrant);
    state.gameLost =
      game.checkCollisions(
        newQuadrantXY,
        newSectorXY,
        state.klingonArr,
        state.starArr,
        state.starfleetArr
      ) || game.checkBoundaries(newQuadrantXY);
    (state.quadrant[0] !== newQuadrantXY[0] ||
      state.quadrant[1] !== newQuadrantXY[1]) &&
      game.randomizeObjectPositions(state, newQuadrantXY);
    if (
      game.checkStarFleetNearby(newSectorXY, newQuadrantXY, state.starfleetArr)
    ) {
      state.energy = config.INIT_ENERGY_LVL;
    } else {
      state.energy -= calcEnergy;
    }
    state.stardates -= calcStardates;
    state.sector = newSectorXY;
    state.quadrant = newQuadrantXY;

    game.checkHostile(state) && game.engage(state, true);
    mode = COMMANDS_MODE;
  } else if (mode === BATTLE_MODE) {
    console.log(`ENTERPRISE HAS ${state.energy} ENERGY`);
    let amountOfEnergy = inputReader.readAmountOfEnergy();
    game.engage(state, false, amountOfEnergy) && game.engage(state, true);
    if (amountOfEnergy === 0) {
      console.log('ENTERING INTO THE COMMANDS MODE');
      mode = COMMANDS_MODE;
    }
  }
}
console.log('Game finished!');
