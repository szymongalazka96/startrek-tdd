import config from './config.js';
import {
  findAllInQuadrant,
  randomXY,
  cityBlockDistance,
  isHit,
} from './utils/helpers.js';

const game = {
  initQuadrant() {
    const Array2D = (r, c) => [...Array(r)].map((x) => Array(c).fill('   '));
    return Array2D(config.QUADRANT_LENGTH, config.QUADRANT_LENGTH);
  },
  initSector() {
    const Array2D = (r, c) => [...Array(r)].map((x) => Array(c).fill(' . '));
    return Array2D(config.SECTOR_LENGTH, config.SECTOR_LENGTH);
  },
  initObjects(size) {
    const objects = [...Array(size)].map((_, i) => {
      return { quadrant: randomXY(), sector: randomXY() };
    });
    return objects;
  },
  checkCollisions(quadrant, player, ...arr) {
    const objectsInQuadrant = arr.map((arr) =>
      findAllInQuadrant(arr, quadrant)
    );
    const collision = objectsInQuadrant.some(
      (arr) =>
        arr.find(
          (obj) => JSON.stringify(obj.sector) === JSON.stringify(player)
        ) ?? false
    );
    return collision;
  },
  checkStarFleetNearby(pos, quadrant, sf) {
    let variants = [];
    pos[0] !== 7 && variants.push([pos[0] + 1, pos[1]]);
    pos[0] !== 0 && variants.push([pos[0] - 1, pos[1]]);
    pos[1] !== 7 && variants.push([pos[0], pos[1] + 1]);
    pos[1] !== 0 && variants.push([pos[0], pos[1] - 1]);
    const sfInside = findAllInQuadrant(sf, quadrant);
    const isNearby = sfInside.some((sf) =>
      variants.some((v) => JSON.stringify(v) === JSON.stringify(sf.sector))
    );
    return isNearby;
  },
  checkBoundaries(pos) {
    return (
      pos[0] >= config.QUADRANT_LENGTH ||
      pos[1] >= config.QUADRANT_LENGTH ||
      pos[0] < 0 ||
      pos[1] < 0
    );
  },
  randomizeObjectPositions(state, quadrant) {
    const { starfleetArr, starArr, klingonArr } = state;
    const kligonsInside = findAllInQuadrant(klingonArr, quadrant);
    const starsInside = findAllInQuadrant(starArr, quadrant);
    const sfInside = findAllInQuadrant(starfleetArr, quadrant);
    kligonsInside.forEach((kligon) => (kligon.sector = randomXY()));
    starsInside.forEach((star) => (star.sector = randomXY()));
    sfInside.forEach((sf) => (sf.sector = randomXY()));
  },
  scanQuadrant(state, quadrant) {
    const { starfleetArr, starArr, klingonArr } = state;
    const kligonsInside = findAllInQuadrant(klingonArr, quadrant);
    const starsInside = findAllInQuadrant(starArr, quadrant);
    const sfInside = findAllInQuadrant(starfleetArr, quadrant);
    return `${kligonsInside.length}${sfInside.length}${starsInside.length}`;
  },
  checkHostile(state) {
    const { klingonArr, quadrant } = state;
    const kligonsInside = findAllInQuadrant(klingonArr, quadrant);
    const isHostileQuadrant = kligonsInside.length > 0;
    isHostileQuadrant &&
      console.log(`${kligonsInside.length} hostile klingons in quadrant!`);
    return isHostileQuadrant;
  },
  engage(state, enterpriseAttacked, amount = 20) {
    const { sector, klingonArr, quadrant } = state;
    const kligonsInside = findAllInQuadrant(klingonArr, quadrant);
    console.log(kligonsInside);
    const distance = cityBlockDistance(
      sector[0],
      sector[1],
      kligonsInside[0].sector[0],
      kligonsInside[0].sector[1]
    );
    if (enterpriseAttacked) {
      if (isHit(distance)) {
        state.energy -= 20;
        console.log('Enterprise damaged!');
      }
      return false;
    } else {
      if (isHit(distance)) {
        state.energy -= amount;
        kligonsInside[0].energy -= amount;
        console.log('Kligon damaged!');
        if (kligonsInside[0].energy <= 0) {
          console.log('Kligon destroyed!');
          kligonsInside.splice(0, 1);
        }
        return true;
      }
      return false;
    }
  },
  drawUI(ui) {
    const regex = /(,|")/gi;
    ui.reverse().forEach((row) =>
      console.log(JSON.stringify(row).replace(regex, ''))
    );
  },
  calculateNewData(sector0, sector1, quadrant0) {
    const calculateNewSectorPos = (distance) =>
      ((distance % config.SECTOR_LENGTH) + config.SECTOR_LENGTH) %
      config.SECTOR_LENGTH;

    const calculateNewQuadrantPos = (distance, currQuadrantPos) => {
      const quadrantPos = Math.floor(distance / config.SECTOR_LENGTH);
      return (currQuadrantPos += quadrantPos);
    };

    const newSectorXY = [
      calculateNewSectorPos(sector0[0] + sector1[0]),
      calculateNewSectorPos(sector0[1] + sector1[1]),
    ];

    const newQuadrantXY = [
      calculateNewQuadrantPos(sector0[0] + sector1[0], quadrant0[0]),
      calculateNewQuadrantPos(sector0[1] + sector1[1], quadrant0[1]),
    ];

    const calcEnergy = cityBlockDistance(
      sector0[0],
      sector0[1],
      sector0[0] + sector1[0],
      sector0[1] + sector1[1]
    );

    const calcStardates = cityBlockDistance(
      quadrant0[0],
      quadrant0[1],
      newQuadrantXY[0],
      newQuadrantXY[1]
    );
    return {
      calcEnergy,
      newSectorXY,
      newQuadrantXY,
      calcStardates,
    };
  },
};

export default game;
