const status = (stardates, energy, quadrantXY, sectorXY) =>
  `==ENERGY{${energy}}==STARDATES{${stardates}}==QUADRANT[${quadrantXY}]==SECTOR[${sectorXY}]==`;
export default status;
