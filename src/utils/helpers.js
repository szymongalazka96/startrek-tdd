import config from '../config.js';

export const cityBlockDistance = (x1, y1, x2, y2) =>
  Math.abs(x1 - x2) + Math.abs(y1 - y2);

export const getRandomInt = (max) =>
  Math.floor(Math.random() * Math.floor(max));

export const randomXY = () => [
  getRandomInt(config.QUADRANT_LENGTH),
  getRandomInt(config.QUADRANT_LENGTH),
];

export const findAllInQuadrant = (arr, quadrant) =>
  arr.filter(
    (item) => JSON.stringify(item.quadrant) === JSON.stringify(quadrant)
  );

export const isHit = (distance) => Math.random() <= 5 / (distance + 4);
